from setuptools import setup

setup(
    name='pynmcli',
    version='0.1',
    packages=['pynmcli'],
    url='',
    license='GPLv3',
    author='',
    author_email='',
    description='A basic python wrapper of the nmcli tool',
)
