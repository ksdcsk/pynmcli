# Python nmcli (Network Manager cli tool) wrapper

This is a basic python wrapper.

## Usage
```python
from pynmcli.connections import get_network_connections

connections_list = get_network_connections()
```