

import subprocess
from typing import List

from .exceptions import NMCliError


def run_nmcli(args: list) -> List[str]:
    command = ['nmcli', '-t'] + args

    proc = subprocess.Popen(
        command,
        shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    # Wait until the subprocess terminates
    proc.wait()

    # If the exit code is not 0, raise an exception
    if proc.returncode != 0:
        raise NMCliError(
            f"nmcli returned the error code {proc.returncode}.",
            proc.returncode,
            _bytes_lines_to_text(proc.stderr.readlines())
        )

    # Get the subprocess output
    proc_output = list(
        map(
            lambda row: row.decode('utf8').strip(),
            proc.stdout.readlines()
        )
    )

    return proc_output


def _bytes_lines_to_text(bytes_lines: List[bytes]) -> str:
    return "\n".join(
        map(
            lambda row: row.decode('utf8').strip(),
            bytes_lines
        )
    )
