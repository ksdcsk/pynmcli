

import re
from typing import List, Union

from ._utils import run_nmcli
from .exceptions import ConnectionTypeNotImplementedError, NMCliError


class Connection:
    _uuid = None
    _props = None

    def __init__(self, connection_id):
        self._uuid = connection_id

        self.refresh()

    def get_uuid(self) -> str:
        return self._uuid

    def get_name(self) -> str:
        return self._props['connection.id']

    def is_up(self) -> bool:
        if 'GENERAL.STATE' in self._props:
            return self._props['GENERAL.STATE'] == 'activated'

        return False

    @staticmethod
    def __exec_cmd_no_output(cmd: list) -> None:
        try:
            run_nmcli(cmd)
        except NMCliError as ex:
            raise ex

    def bring_up(self) -> None:
        self.__exec_cmd_no_output(['connection', 'up', self._uuid])

    def bring_down(self) -> None:
        self.__exec_cmd_no_output(['connection', 'down', self._uuid])

    def refresh(self) -> None:
        connection_raw_data = run_nmcli(['connection', 'show', self._uuid])

        props_parser = re.compile(
            r'^(?P<name>.*?(?<!\\)):(?P<value>.*?(?<!\\))$'
        )

        props = {}
        for row in connection_raw_data:
            prop_match = props_parser.match(row)

            props[prop_match.group('name')] = prop_match.group('value')

        self._props = props

    def __repr__(self):
        return f"Connection <{self._uuid}>"


class EthernetConnection(Connection):
    def __init__(self, connection_id):
        super().__init__(connection_id)

        # Check that the connection is of the right type
        if self._props['connection.type'] != '802-3-ethernet':
            raise RuntimeError("Illegal internal state found.")


class VlanConnection(Connection):
    def __init__(self, connection_id):
        super().__init__(connection_id)

        # Check that the connection is of the right type
        if self._props['connection.type'] != 'vlan':
            raise RuntimeError("Illegal internal state found.")

    def parent_id(self) -> str:
        return self._props['vlan.parent']


def get_network_connections(ignore_unknown_connection_types=False) -> List[Union[EthernetConnection, VlanConnection]]:
    connections_list_nmcli = run_nmcli(['connection'])
    connections_parser = re.compile(
        r'^(?P<name>.*?(?<!\\)):(?P<uuid>.*?(?<!\\)):(?P<type>.*?(?<!\\)):(?P<device>.*?(?<!\\))$'
    )

    connections = []
    for connection_unformatted in connections_list_nmcli:
        connection_match = connections_parser.match(connection_unformatted)

        if connection_match.group("type") == "802-3-ethernet":
            connections.append(
                EthernetConnection(connection_match.group("uuid"))
            )
        elif connection_match.group("type") == "vlan":
            connections.append(
                VlanConnection(connection_match.group("uuid"))
            )
        else:
            if ignore_unknown_connection_types:
                continue
            else:
                raise ConnectionTypeNotImplementedError(
                    f"Connection type <{connection_match.group('type')}> not implemented."
                )

    return connections
