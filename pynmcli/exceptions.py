

class ConnectionTypeNotImplementedError(NotImplementedError):
    pass


class NMCliError(Exception):
    _details: str = None
    _exit_code: int = None

    def __init__(self, error_msg: str, exit_code: int, details: str):
        super().__init__(error_msg)

        self._details = details
        self._exit_code = exit_code

    def get_details(self) -> str:
        return self._details

    def get_exit_code(self) -> int:
        return self._exit_code
